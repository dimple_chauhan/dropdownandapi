//
//  ViewController.swift
//  DropDownAndAPI
//
//  Created by Kapil Dhawan on 24/01/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit


    


class ViewController: UIViewController{
    var countriesName: [String] = []
    var countriesCode: [String] = []
    var stateName: [String] = []
    var stateCode: [String] = []
    var cityName: [String] = []
    var cityCode: [String] = []
    let apiPath = "https://stsmentor.com/locationapi/locationApi.php?type="
   
    
    let getCountries = "getCountries"
    let getStates = "getStates"
    let getCities = "getCities"
    
    let countryId = "countryId"
    let stateId = "stateId"
    var countryCode1 = ""
    var stateCode1 = ""
   
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var tableCountry: UITableView!
    
    @IBOutlet weak var btnState: UIButton!
    @IBOutlet weak var tableState: UITableView!
    
    @IBOutlet weak var btnCity: UIButton!
    @IBOutlet weak var tableCity: UITableView!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableCountry.delegate = self
       tableCountry.dataSource = self
        tableState.delegate = self
        tableState.dataSource = self
        tableCity.delegate = self
        tableCity.dataSource = self
        
        
        }
 
   
    @IBAction func btnCountryClicked(_ sender: Any) {
        let countryPath = "\(apiPath)\(getCountries)"
        guard let url = URL(string: countryPath) else { return }
        
        URLSession.shared.dataTask(with: url){  (data,response,error) in
            guard let data = data else { return }
            
            do{
                let response = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
                let result = response["result"] as! Array<Any>
                for re in result{
                    for rs in re as! NSDictionary{
                        self.countriesName.append(rs.value as! String)
                        self.countriesCode.append(rs.key as! String)
                    }
                }
                DispatchQueue.main.async {
                    self.tableCountry.reloadData()
                }
                
            }
            catch let jsonerror{
                print("Error: ", jsonerror)
            }
            }.resume()
        
        if tableCountry.isHidden{
            animateCountry(toogle: false)
        }else{
            animateCountry(toogle: true)
        }
        
    }
    
    func animateCountry(toogle: Bool){
        if toogle{
            UIView.animate(withDuration: 0.0) {
                self.tableCountry.isHidden = true
            }}
        else{
            UIView.animate(withDuration: 0.0) {
                self.tableCountry.isHidden = false
            }
        }
    }
    
  
    
    @IBAction func btnStateClicked(_ sender: Any) {
        let statePath = "\(apiPath)\(getStates)&\(countryId)=\(countryCode1)"
        guard let url = URL(string: statePath) else { return }
        
        URLSession.shared.dataTask(with: url){  (data,response,error) in
            guard let data = data else { return }
            
            do{
                let response = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
                let result = response["result"] as! Array<Any>
                self.stateName.removeAll()
                self.stateCode.removeAll()
                for re in result{
                    for rs in re as! NSDictionary{
                        self.stateName.append(rs.value as! String)
                        self.stateCode.append(rs.key as! String)
                    }
                }
              
                DispatchQueue.main.async {
                    self.tableState.reloadData()
                }
                
            }
            catch let jsonerror{
                print("Error: ", jsonerror)
            }
            }.resume()
        
        if tableState.isHidden{
            animateState(toogle: false)
        }else{
            animateState(toogle: true)
        }
        
    }
    
    func animateState(toogle: Bool)
    {
        if toogle{
            UIView.animate(withDuration: 0.0) {
                self.tableState.isHidden = true
            }}
        else{
            UIView.animate(withDuration: 0.0) {
                self.tableState.isHidden = false
            }
        }
    }
    
    
    @IBAction func btnCityClicked(_ sender: Any) {
        
        let cityPath = "\(apiPath)\(getCities)&\(stateId)=\(stateCode1)"
        guard let url = URL(string: cityPath) else { return }
        
        URLSession.shared.dataTask(with: url){  (data,response,error) in
            guard let data = data else { return }
            
            do{
                let response = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
                let result = response["result"] as! Array<Any>
                self.cityName.removeAll()
                self.cityCode.removeAll()
                for re in result{
                    for rs in re as! NSDictionary{
                        self.cityName.append(rs.value as! String)
                        self.cityCode.append(rs.key as! String)
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableCity.reloadData()
                }
                
            }
            catch let jsonerror{
                print("Error: ", jsonerror)
            }
            }.resume()
        
        if tableCity.isHidden{
            animateCity(toogle: false)
        }else{
            animateCity(toogle: true)
        }
        
    }
    
    func animateCity(toogle: Bool)
    {
        if toogle{
            UIView.animate(withDuration: 0.0) {
                self.tableCity.isHidden = true
            }}
        else{
            UIView.animate(withDuration: 0.0) {
                self.tableCity.isHidden = false
            }
        }
    }
    
}




extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableCountry{
        return self.countriesName.count
        }else  if tableView == self.tableState{
            return self.stateName.count
        }else{
            return self.cityName.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableCountry{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath)
            cell.textLabel?.text = self.countriesName[indexPath.row]
            return cell
        }else if tableView == self.tableState{
            let cell = tableView.dequeueReusableCell(withIdentifier: "StateCell", for: indexPath)
            cell.textLabel?.text = self.stateName[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
            cell.textLabel?.text = self.cityName[indexPath.row]
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableCountry{
        btnCountry.setTitle("\(self.countriesName[indexPath.row])", for: .normal)
        animateCountry(toogle: true)
        countryCode1 = String(self.countriesCode[indexPath.row])
            
        }else  if tableView == self.tableState{
            btnState.setTitle("\(self.stateName[indexPath.row])", for: .normal)
            animateState(toogle: true)
            stateCode1 = String(self.stateCode[indexPath.row])
        }
        else{
            btnCity.setTitle("\(self.cityName[indexPath.row])", for: .normal)
            animateCity(toogle: true)
       
        }
    
    }

        
    
}

